//
//  RaiseUserEvent.h
//  MagicApp
//

@interface RaiseUserEvent : NSObject

+(void)raiseEvent:(NSArray *)params;

@end
