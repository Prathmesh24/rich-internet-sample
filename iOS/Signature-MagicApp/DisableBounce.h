//
//  DisableBounce.h
//  MagicApp
//

@interface DisableBounce : NSObject

+(void)disableBounce:(NSArray *)params;

@end
