//
//  ImageResize.h
//  MagicApp
//

@interface ImageResize : NSObject

+(NSString *)resize:(NSArray *)params;
+(NSString *)rotate:(NSArray *)params;

@end
